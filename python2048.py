#Bibliothèques graphique (natives avec python 3)
from tkinter import *
from tkinter.messagebox import *
import random

#Bibliothèque de l'aléa (native avec python 3)
#Pour appeler un nombre entier au hasard entre a et b 
#on fait poetiquement : randint(a, b)
from random import randint



#Ca fait plaisir de signer son travail ! Mettez à jour cette variable ^_^
auteur="Hajar Oumerri"

"""
*********************************************************************************
*																				*
*							PROGRAMMATION DU JEU 2048							*
*																				*
*********************************************************************************
	Mais bien sur que vous connaissez ce jeu dont l'objectif est d'arriver 
	à la puissance 11 du nombre préféré d'un informaticien : 2048 (2^11).
	
	Lorsque la flèche gauche est utilisée, tous les nombres se retrouvent projeter
	sur la gauche et une règle s'applique : après ce mouvement tous les nombres identiques 
	sur la gauche fusionnent et passent à la puissance supérieur. Voici des exemples : 
		
		2 4 X 4 8 X <-
			deviens : 2 8 8 X X X
			(hé oui, le 8 est crée mais il n'a pas été encore poussé pour fusionner avec l'autre)
			
		2 2 X 2 X 2 <-
			deviens : 4 4 X X X X
			(même principe, les 4 ne peuvent pas fusionner parce qu'ils viennent d'apparaitre)
			
		16 16 16 X X X <-
			deviens : 32 16 X X X X
			(quand il y a "égalité" on va dire que c'est contre le rebord que ça fusione)
			(cela ne fait donc pas 16 32 X X X X)
			
	Bien sur c'est le même principe par un mouvement sur la droite, en haut et en bas.
	
	La partie se termine dès qu'on atteint le nombre 2048 (et on gagne) 
	ou si plus aucun mouvement n'est possible (et on perd) et que la grille est complète !
		
	Pour que ça soit sympa, l'interface graphique est donné (gratuit, c'est bibi qui offre)
	Ceci implique QUE VOUS NE POUVEZ PAS SUPPRIMER LES FONCTIONS CI-DESSOUS.
	Mais rien ne vous interdit de les modifier (d'ailleurs il FAUT les modifier)
	Une seule exception : PAS TOUCHE A LA FONCTION 'FENETRE' qui gère la fenètre graphique !
	Pour le reste : 
		- a vous de modifier les autres fonctions existantes (sans modifier le type d'entrée et de sortie)
		- d'introduire d'autre fonctions ou procédures (autant que vous voulez, aucune restriction)
		- de divisier ce projet en plein de sous tache peut-etre plus facile
		- de commenter votre code (même pour vous ça sera plus confortable)
		
	Allez ! Une petite pièce SOS pour vous guider : 
		la grille de jeu sera assimillé à un tableau à deux entrées 
							grille[i][j]
		le premier indice (i) correspondant à la ligne (de haut en bas)
		le second indice (j) correspondant à la colonne (de gauche à droite)
		Une case vide sera représenté par le nombre 0 (l'interface ne l'affiche pas)
		pour faire simple tous les tableaux seront des dictionnaires 
		(comme ça on se prend pas la tête à jouer avec la mémoire)

"""

#Initialisation : début de partie

def Init(dim) :   #permet de commencer le jeu avec deux tuiles qui portent soit les valeurs 2 ou 4 placées aleatoirement, 
	X=dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim):
			X[i][j]=0
	var = 0
	while var != 2:
		i = randint(0,dim-1)
		j = randint(0,dim-1)
		while X[i][j] != 0:
			i = randint(0,dim-1)
			j = randint(0,dim-1)
			X[i][j]=random.choice([2,4])
		X[i][j]=random.choice([2,4])
		var+=1
	

	return X


#def Ajout(X): #prend en parametre la grille, elle permet d'ajouter une valeur dans une grille au hasard à chaque fois que le jouer fait un deplacement 
	#dim = len(X)   #on doit definir cette variable car la fonction n'y a pas accès
	#i = randint(0,dim-1)
	#j = randint(0,dim-1)
	#if X[i][j] == 0:
		#X[i][j]=random.choice([2,4])

	##La fonction ajout sera appelée dans chaque action de mouvement
def Ajout(X):#cette fonction prend le rôle du joueur 'ordinateur' qui va ajouter une tuile après chaque action du joueur ayant engendré un mouvement sur la grille......SI AUCUN MOUVEMENT: AUCUN AJOUT
	for boucle in range(1) :
		ligne = random.randrange(dim) #choisi une ligne au hasard
		colonne = random.randrange(dim) #choisi une colonne au hasard
		while X[ligne][colonne] != 0 :    ############################################################
			ligne = random.randrange(dim) # Ici ci une case n'est pas vide on ne la touche pas #Si elle est vide on ajoute un 2 ou un 4 (toujours avec les memes proba)
			colonne = random.randrange(dim) #######################################################""
		X[ligne][colonne] = random.choice([2,2,2,2,2,2,2,2,2,2,2,4]) #Si elle est vide on ajoute un 2 ou un 4
	return X #parce que toutes les autres fonctions retournent X
	
	
	



#fonction qui permettra de calculer les tuiles qui vont rentrer en fusion lors des deplacements
#def fusion():
        #score=0
        #i=0
        #for i < dim:
                #j=0
               # for j< dim:
                       # if X[i][j]==X[i+1][j]:
                               # X[i][j]=X[i][j]+X[i+1][j]
                               # score+=X[i][j]
                                #X[i+1][j]=0
			
	#return score




#Renvoie 1 si c'est gagné
#Renvoie -1 si c'est perdu
#Renvoie 0 sinon
                               #parcourir le dictionnaire

def TestFin(grille) : #permet de tester si c'est la fin du jeu
	dim = len(grille) #il faut parcourir la grille 
	X = dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim) :
			X[i][j]=grille[i][j]
			if X[i][j] == 2048 :        #si une case est égale à 2048 la partie est gagnée
				return 1
			else:
				return 0
				
	return X

def FusionDroite(grille):
	dim=len(grille)
	i=0
	score=0
	while i< dim:
		j=dim-1
		while j!=0:
			if X[i][j]==X[i][j-1]:
				X[i][j]=X[i][j]+X[i][j-1]
				score+=X[i][j]
				X[i][j-1]=0
				score+=1
			j-=1
		i+=1
			
	return X

        #dim= len(grille)
	#calcul_effectuer = False #Au départ aucun calcul n'a été effectué
	#for i in range(dim):
		#for j in range(dim):
			#if X[i][j] == X[i][j+1]:    #Si notre tuile est égale à celle qui se trouve à sa droite
			#	X[i][j+1] = X[i][j+1] * 2  #Alors celle de droite prends le double de sa valeur
			#	X[i][j] = 0
			#	calcul_effectuer = True #Et notre tuile vaut désormais 0
	#return X
	
	

#Renvoie la grille mise à jour après un mouvement vers le haut
#En entrée : la grille avant le mouvement
#En sortie : la grille après le mouvement
#def ActionHaut(grille) :
	#dim=len(grille)
	#X=dict()
	#for i in range(dim) :
		#X[i]=dict()
		#for j in range(dim) :
			#X[i][j]=grille[i][j]
	

	#Ajout(X)
	#fusion()
	
	
	#modif=True
	#while modif:
		#modif=False       #on met modif a False
		#for i in range(dim):
			#for j in range(dim):
				#if grille[i][j]!= 0 and grille[i-1][j]== 0:
					#if grille[i-1][j]==0:
					#grille[i-1][j] = grille[i][j]
					#grille[i][j] = 0
					#modif=True   #là on le  remet
					#if i == 0:
						#break
			
	#return X


#Renvoie la grille mise à jour après un mouvement vers le bas
#En entrée : la grille avant le mouvement
#En sortie : la grille après le mouvement	
def ActionBas(grille) :
	dim=len(grille)
	X=dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim) : 
			X[i][j]=grille[i][j]

	j = 0
	while j < dim:
		i = dim-1
		while i >= 0:
			pos= dim-1
			j
			if X[i][j]!= 0:
				X[k][j]=X[i][j]
				if k > i:
					X[i][j]=0  
					k-=1
			i-=1
		j+=1
					

	Ajout(X)
	

	return X
		
#Renvoie la grille mise à jour après un mouvement vers la gauche
#En entrée : la grille avant le mouvement
#En sortie : la grille après le mouvement
def ActionGauche(grille) :
	dim=len(grille)
	X=dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim):
			X[i][j]=grille[i][j]
	#Ici je parcours le tableau en commençant par l'avant dernière case
	#Tant que la case suivante seras égale a zero je switch
	i = 0
	
	while i != dim: #Pour parcourir chaque ligne
		j = 1 #j'initialise le j a l'avant dernière case a chaque ligne qu'il va explorer
		while j <dim:
			
			while X[i][j] != 0 and X[i][j - 1] == 0: 
				X[i][j-1] = X[i][j]
				X[i][j] = 0
				j += 1
	
				if j == dim:
					break
			j += 1
		i+=1
		
	
					

	Ajout(X)
	
			
	return X
	
#Renvoie la grille mise à jour après un mouvement vers la droite
#En entrée : la grille avant le mouvement
#En sortie : la grille après le mouvement
def ActionDroite(grille) :
	dim=len(grille)
	X=dict()
	for i in range(dim) :
		X[i]=dict()
		for j in range(dim) : 
			X[i][j]=grille[i][j]

	i = 0
	
	while i< dim:
		pos= dim-1
		j = pos-1
		while j !=0 :
			if X[i][j]!= 0:
				X[i][pos]=X[i][j]
				X[i][j]=0
				pos= pos-1
			
			j-=1
		i+=1
					

	#Ajout(X)
	#FusionDroite(X)
	
	return X



#-----------------------------------------NE PAS TOUCHER À CETTE FONCTION--------------------------------------
def FENETRE(dim) :

	def recupPartie() :
		dim=len(JEU)
		X=dict() 
		for i in range(dim) :
			X[i]=dict()
			for j in range(dim) :
				try : X[i][j]=int(JEU[i][j].get())
				except : X[i][j]=0
		return X
		
	def InjectionPartie(X) : 
		for i in range(dim) :
			for j in range(dim) :
				try : (JEU[i][j]).set(X[i][j])
				except : (JEU[i][j]).set(0)
				
				if(int(JEU[i][j].get())==0) : (JEU[i][j]).set("")

	def Clavier(mouvement):
		CMPT.set(CMPT.get()+1)
		
		touche = mouvement.keysym
		# déplacement vers le haut
		if touche == 'Up': ActionHaut0()
		# déplacement vers le bas
		if touche == 'Down': ActionBas0()
		# déplacement vers la droite
		if touche == 'Right': ActionDroite0()
		# déplacement vers la gauche
		if touche == 'Left': ActionGauche0()
		
	def ActionRecommencer() :
		X=Init(dim)
		for i in range(dim) :
			for j in range(dim) : 
				(JEU[i][j]).set(X[i][j])
				if(X[i][j]==0) : (JEU[i][j]).set("")
		CMPT.set(0)
		
	def ActionHaut0() :
		InjectionPartie(ActionHaut(recupPartie()))
		TestFin0(recupPartie())
			
	def ActionBas0() :
		InjectionPartie(ActionBas(recupPartie()))
		TestFin0(recupPartie())
		
	def ActionGauche0() :
		InjectionPartie(ActionGauche(recupPartie()))
		TestFin0(recupPartie())
			
	def ActionDroite0() :
		InjectionPartie(ActionDroite(recupPartie()))
		TestFin0(recupPartie())
	
	def TestFin0(X) :			
		test=TestFin(X)
		if(test==1) : showinfo("FIN DE PARTIE", "Bravo ! Vous avez gagné en "+str(CMPT.get())+" coups. Recommencer ?")
		if(test==-1) :showinfo("FIN DE PARTIE", "Perdu ! Il vous a fallu "+str(CMPT.get())+" coups pour perdre... comment dire. On s'arrête là ou on recommence ?")
		if(test!=0) : ActionRecommencer()
	
	fenetre = Tk()
	fenetre.title('2048 par '+auteur)
	
	CMPT=IntVar()
	CMPT.set(0)
	
	X=Init(dim)
	JEU=dict()
	for i in range(dim) :
		JEU[i]=dict()
		for j in range(dim) : 
			JEU[i][j]=StringVar()
			(JEU[i][j]).set(X[i][j])
			if(X[i][j]==0) : (JEU[i][j]).set("")
			
	base=70 #Taille en px d'un carré
	if(dim>7) : base=50
	marge=10 #Marge de beauté

	hauteur = dim*base+2*marge+100
	largeur = dim*base+2*marge

	canvas = Canvas(fenetre, background="orange", width=largeur, height=hauteur )

	case=dict()
	for i in range(dim) :
		case[i]=dict()
		for j in range(dim) : 
			canvas.create_rectangle((base*i+marge,base*j+marge), (base*(i+1)+marge,base*(j+1)+marge), width=1)

			c_fg='black'
			c_bg ='orange' 
			
			if((JEU[i][j]).get()==0) : c_fg='white'
			
			L=Label(fenetre, textvariable=JEU[i][j], fg=c_fg, bg=c_bg)
			L.place(x=base*j+base//2, y=base*i+base//2, anchor="nw")
			

	txt="Nombre de mouvement : "
	L=Label(fenetre, text=txt, fg='black', bg='white')
	L.place(x=marge, y=(hauteur-base), anchor="sw")
	L=Label(fenetre, textvariable=CMPT, fg='black', bg='white')
	L.place(x=marge+len(txt)*7, y=(hauteur-base), anchor="sw")

	fenetre.geometry(str(largeur)+"x"+str(hauteur))
	BoutonQuitter = Button(fenetre, text ='Quitter', command = fenetre.destroy)
	BoutonQuitter.place(x=marge, y=hauteur-marge, anchor="sw")
	BoutonRecommencer = Button(fenetre, text ='Recommencer', command = ActionRecommencer)
	BoutonRecommencer.place(x=largeur-marge, y=hauteur-marge, anchor="se")

	canvas.focus_set()
	canvas.bind('<Key>',Clavier)
	
	canvas.grid()
	fenetre.mainloop()
#---------------------------------------------------------------------------------------------------------------
#Dimension du 2048 - usuellement 4
dim=0
while(dim<3 or dim>10) :
	try : dim=int(input("Quelle taille votre 2048 (entre 3 et 10): "))
	except : dim=0


#Fonction principale	
FENETRE(dim)


"""
*********************************************************************************
*																				*
*					PROGRAMMATION DU JEU 2048 - EVOLUTION						*
*																				*
*********************************************************************************
	Hein ! Quoi ! Vous avez fini en avance et vous voulez avancer un petit
	peu plus ! Génial. Si vous alliez explorer les tutoriels d'explications 
	de cette étrange bibliothèque tkinter... peut-être que vous trouverez
	un truc pour rajouter un peu de couleur ou une petite musique de fond ^_^
	A vous de voir !
	
	Hé sinon, si au lieu de faire apparaitre des puissances de 2 succéssives
	on mettait les termes de la suite de Fibonacci (1 2 3 5 8 13 etc)
	Et si on laissait l'utilisateur choisir son mode de jeu : 
	puissance de 2 ou suite de Fibonacci ;-)
	
	Qu'est-ce qu'on pourrait encore améliorer !?
	A vous de me faire une surpise, mais n'oubliez pas de préparer la
	page web pour la soutenance !

"""
